const prompt = require("prompt-sync")();

//Exercice 1 : 
//#region 
console.log("Exercice 2 : ");
console.log(" ");
const prenom = prompt("Veuillez saisir votre prénom : ");
console.log(`Vous avez saisi : ${prenom}`);

const nom = prompt("Veuillez saisir votre nom : ");
console.log(`Vous avez saisi : ${nom}`);

console.log("Bonjour " + prenom + " " + nom);
//#endregion

//Exercice 2 : 
//#region 
console.log("Exercice 2 : ");
console.log(" ");
var nbr1 = 0, nbr2=0, resultat = 0;

nbr1 = Number(prompt("Saisir le premier nombre : "));
nbr2 = Number(prompt("Saisir le second nombre : "));

resultat = nbr1 + nbr2;

console.log("La somme des nombres " + nbr1 + " et " + nbr2 + " est de " + resultat);
//#endregion

//Exercice 3 : 
//#region 
console.log("Exercice 3 : ");
console.log(" ");

var diametre, perimetre, aire, nb1,nb2=0, res;
const pi = Math.PI;

console.log("Le calcul du perimètre d'un cercle et de son aire : ")
console.log(" ");
diametre = Number(prompt("Veuillez saisir le diamètre du cercle : "));
perimetre = diametre*pi;
aire = pi*Math.pow((diametre/2),2);

console.log(`Le diamètre est de : ${diametre}`);
console.log(`Le périmètre est de : ${Math.round(perimetre)}`);
console.log(`L'aire est de : ${Math.round(aire)}`);
//#endregion

//Exercice 4 : 
//#region 
console.log("Exercice 4 : ");
console.log(" ");
var mot, motLower, reverse;
console.log("Ce mot est il un palindrome ? ")
console.log(" ")
mot = prompt("Veuillez saisir un mot : ");
motLower = mot.toLowerCase();
console.log(" ");
console.log("Vous avez saisi : " + mot);
reverse = motLower.split('').reverse().join('');
console.log("Le mot inversé est : " + reverse);

if (motLower === reverse){

console.log("Le mot " + mot + " est un palindrome");

}else{
    console.log("Le mot " + mot + " n'est pas un palindrome");
}
//#endregion

//Exercice 9 : 
//#region 
console.log("Exercice 9 : ");
console.log(" ");

var capitalInitial = 0, tauxInteret = 0, duree= 0, capitalFinal = 0, interet = 0;
capitalInitial = prompt("Saisir votre capital initial : ");
tauxInteret = prompt("Saisir votre taux d'interêt : ");
duree = prompt("Saisir la durée de l'epargne : ");

capitalFinal = Math.round(capitalInitial * Math.pow(1 + (tauxInteret/100), duree),2);
interet = Math.round(capitalFinal - capitalInitial,2);

console.log("Avec un capital initial de " + capitalInitial + " , placé avec un taux de  " + tauxInteret + "% pendant " + duree + " année(s)" );
console.log(" ");
console.log("Le montant total des interêts s'elevera à " + interet);
console.log(" ");
console.log("Le capital final à l'issue sera de " + capitalFinal);
//#endregion




